﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BruteVsAlg;

namespace DateAlgorithmTest
{
    /// <summary>
    /// This test is desinged to show the sustancial difference between bruteforce algorithm and mathematical solutions.
    /// Check the test ms results for TestBruteSpeed, TestBruteSpeedMultiThread and TestAlgSpeed to see the differences.
    /// Watch out your loops !
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestOneWeekendAlg()
        {
            DateClass dobj = new DateClass();

            DateTime a = new DateTime(2018, 5, 15);
            DateTime b = dobj.CalculateDate(a, 5);

            Assert.IsTrue(b == new DateTime(2018, 5, 22));
        }

        [TestMethod]
        public void TestThreeWeeksAlg()
        {
            DateClass dobj = new DateClass();

            DateTime a = new DateTime(2018, 5, 15);
            DateTime b = dobj.CalculateDate(a, 21);

            Assert.IsTrue(b == new DateTime(2018, 6, 13));
        }

        [TestMethod]
        public void TestOneWeekendBrute()
        {
            DateClass dobj = new DateClass();

            DateTime a = new DateTime(2018, 5, 15);
            DateTime b = dobj.CalculateDateBruteForce(a, 5);

            Assert.IsTrue(b == new DateTime(2018, 5, 22));
        }

        [TestMethod]
        public void TestThreeWeeksBrute()
        {
            DateClass dobj = new DateClass();

            DateTime a = new DateTime(2018, 5, 15);
            DateTime b = dobj.CalculateDateBruteForce(a, 21);

            Assert.IsTrue(b == new DateTime(2018, 6, 13));
        }

        [TestMethod]
        public void TestBruteSpeed()
        {
            DateClass dobj = new DateClass();

            DateTime a = new DateTime(2018, 5, 15);

            for (int i = 0; i < 10000; i++)
            {
                DateTime b = dobj.CalculateDateBruteForce(a, 7 * i);
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestBruteSpeedMultiThread()
        {
            DateClass dobj = new DateClass();

            DateTime a = new DateTime(2018, 5, 15);

            for (int i = 0; i < 10000; i++)
            {
                DateTime b = dobj.CalculateDateBruteForceMultiThread(a, 7 * i);
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestAlgSpeed()
        {
            DateClass dobj = new DateClass();

            DateTime a = new DateTime(2018, 5, 15);

            for (int i = 0; i < 10000; i++)
            {
                DateTime b = dobj.CalculateDate(a, 7 * i);
            }
            Assert.IsTrue(true);
        }

    }
}
