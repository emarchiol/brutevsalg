﻿using System;
using System.Threading;
using System.Threading.Tasks;
namespace BruteVsAlg
{
    public class DateClass
    {
        /// <summary>
        /// Add the dayAmount to the initialDate skipping weekends.
        /// </summary>
        /// <param name="initialDate"></param>
        /// <param name="dayAmount"></param>
        /// <returns>The new datetime with skipped weekends.</returns>
        public DateTime CalculateDate(DateTime initialDate, int dayAmount)
        {
            int amountOfFullWeeks = 0;

            // If I'm already on weekends then I'll skip to monday.
            if (initialDate.DayOfWeek == DayOfWeek.Saturday)
            {
                initialDate.AddDays(2);
            }

            if (initialDate.DayOfWeek == DayOfWeek.Sunday)
            {
                initialDate.AddDays(1);
            }

            amountOfFullWeeks = (dayAmount / 5);
            dayAmount += amountOfFullWeeks * 2;

            if (amountOfFullWeeks > 0)
                initialDate = initialDate.AddDays(dayAmount);

            int dayNumber = (int)initialDate.DayOfWeek;

            if (dayNumber > 5)
            {
                // weekend, skip to monday.
                initialDate = initialDate.AddDays(dayNumber - 5);
            }

            return initialDate;
        }

        /// <summary>
        /// Calculate via bruteforce.
        /// </summary>
        /// <param name="initialDate"></param>
        /// <param name="dayAmount"></param>
        /// <returns></returns>
        public DateTime CalculateDateBruteForce(DateTime initialDate, int dayAmount)
        {
            for (int i = 0; i < dayAmount; i++)
            {
                initialDate = initialDate.AddDays(1);
                if (initialDate.DayOfWeek == DayOfWeek.Saturday || initialDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    initialDate = initialDate.AddDays(1);
                    if (initialDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        initialDate = initialDate.AddDays(1);
                    }
                }
            }

            return initialDate;
        }

        /// <summary>
        /// Calculate via bruteforce with multithread for loop.
        /// </summary>
        /// <param name="initialDate"></param>
        /// <param name="dayAmount"></param>
        /// <returns></returns>
        public DateTime CalculateDateBruteForceMultiThread(DateTime initialDate, int dayAmount)
        {
            int i = 0;
            Parallel.For (i, dayAmount, 
            index => {
                initialDate = initialDate.AddDays(1);
                if (initialDate.DayOfWeek == DayOfWeek.Saturday || initialDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    initialDate = initialDate.AddDays(1);
                    if (initialDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        initialDate = initialDate.AddDays(1);
                    }
                }
            }
            );

            return initialDate;
        }
    }
}